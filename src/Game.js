import Shell from './Shell'
import Player from './Player'
import Flock from './Flock'
import Sparkles from './Sparkles'
import Status from './Status'
import Water from './Water'
import Bullets from './Bullets'
import Curtain from './Curtain'

import {
  playerEdge,
  playerShell,
  playerWater,
  bulletsErrbody
} from './_collisions'

import { buildCurtain, buildEndCredits } from './_dom'

export default function Game() {
  global.round = 0
  const fps = 30
  const status = new Status()
  const bullets = new Bullets(status)
  const shell = new Shell(bullets, status)
  const player = new Player(bullets)
  const sparkles = new Sparkles()
  const water = new Water(bullets)
  const curtain = new Curtain()

  // let flock = new Flock(global.round, player) // start null!
  let flock = null
  let omw = false

  const _nextRound = () => {
    global.round++
    if (global.round === 1) {
      flock = new Flock(player)
      status.startStatus()
      setTimeout(() => {
        shell.eagles1Msg(water)
      }, 2000)
    }
    if (global.round === 2) {
      water.isSplashable = false
      player.canSplash = false
      setTimeout(() => {
        shell.eagles2Msg(water, player)
      }, 2000)
    }
    if (global.round === 3) {
      setTimeout(() => {
        shell.eagles3Msg(water)
      }, 2000)
      player.canSplash = false
    }
    if (global.round === 4) {
      setTimeout(() => {
        shell.eagles4Msg()
      }, 2000)
      shell.stopWriting()
    }
    if (global.round === 5){
      player.controllable = false
      setTimeout(() => {
        shell.finaleEageMsg()
      }, 2000)
      shell.stopBrainstorming()
    }
    if (global.round === 6){
      finalEagle.update = () => {}
      player.update = () => {}
      document.getElementById('shell').style.zIndex = 5
      setTimeout(() => {
        curtain.lowerCurtain(flock.finalEagle, shell.endCreditMsg)
      }, 200)
      setTimeout(() => {
        shell.finalWords()
      }, 1200)
      status.setFinalMessage()
    }
    console.log(`global.round is ${global.round}`)
  }

  const _manageGame = () => {
    if (!flock && !omw && global.round > 0) {
      omw = true
      setTimeout(() => {
        _nextRound()
        flock = new Flock(player)
        omw = false
      }, 2000)
    } else if (global.round < 4 && flock && flock.callNextFlock()) {
      flock = null
    } else if (global.round === 4 && flock && flock.finalEagle && flock.finalEagle.locked) {
      _nextRound()
    } else if (global.round === 5 && flock && flock.finalEagle.cya) {
      _nextRound()
    }
  }

  const _collide = () => {
    playerEdge(player, global.width, global.height)
    playerShell(player, shell, status)
    playerWater(player, water, status)
    bulletsErrbody(bullets, player, water, flock)
  }

  const _update = () => {
    player.update()
    flock && flock.update()

    sparkles.update()
    water.update()
    bullets.update()
  }

  const _draw = () => {
    player.draw()
    flock && flock.draw()
    shell.draw()
    water.draw()
    bullets.draw()
  }

  setInterval(() => {
    _manageGame()
    _collide()
    _update()
    _draw()
  }, 1000 / fps)

  let started = false
  const startGame = () => {
    curtain.buildStartScreen()
    document.addEventListener('keydown', (e) => {
      if (e.keyCode === 32 && !started) {
        started = true
        curtain.removeStartScreen()
        curtain.raiseCurtain(_nextRound)
      }
    })
  }

  startGame()
  // _nextRound() //turn me off
  return this
}
