module.exports.bindPlayerInputs = (player) => {
  document.addEventListener('keydown', (e) => {
    if (player.controllable) {
      switch (e.keyCode) {
        case 65:
        case 37:
          player.left = true
          break
        case 87:
        case 38:
          player.up = true
          break
        case 68:
        case 39:
          player.right = true
          break
        case 83:
        case 40:
          player.down = true
          break
      }
    }
  })

  document.addEventListener('keyup', (e) => {
    if (player.controllable) {
      switch (e.keyCode) {
        case 65:
        case 37:
          player.left = false
          break
        case 87:
        case 38:
          player.up = false
          break
        case 68:
        case 39:
          player.right = false
          break
        case 83:
        case 40:
          player.down = false
          break
      }
    }
  })

  document.addEventListener('keydown', (e) => {
    if (e.keyCode == 32 && (player.canSplash || player.bullet)) {
      player.fireBullet()
    }
  })
}
