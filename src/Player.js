import { bindPlayerInputs } from './_keybindings'
import { teardown, buildGameDiv, position } from './_dom'
import { moveToTarget } from './_animations'

export default function Player(bullets){
  this.x = global.left + global.gameSize/2
  this.y = global.height + global.gameSize - 90
  this.width = global.arcadeMode ? 55 : 45
  this.height = global.arcadeMode ? 55 : 45
  this.speed = global.arcadeMode ? 6 : 4

  this.left = false
  this.right = false
  this.up = false
  this.down = false

  this.bullet = null
  this.shotFired = false
  this.canSplash = false
  this.partnered = false

  this.animating = false
  let canFire = true
  this.controllable = true

  this.squid = buildGameDiv({
    id: 'player',
    src: 'assets/squid.png',
    left: this.x + 8,
    top: this.y + 8,
    width: this.width,
    height: this.height,
  })

  let hearts = []
  this.draw = () => {
    position(this.squid, { top: this.y + 8, left: this.x + 8 })
    if (this.left || this.right || this.up || this.down) {
      this.squid.classList.add('playerWobble')
    } else {
      this.squid.classList.remove('playerWobble')
    }
    hearts.forEach((heart) => {
      position(heart, { left: this.x + heart.x + (global.arcadeMode ? 0 : 8), top: this.y + heart.y + (global.arcadeMode ? 0 : 2)})
    })
  }

  this.update = () => {
    if (this.bullet) {
      this.bullet.x = this.x + 14
      if (this.bullet.bobbingUp){
        this.bullet.y -= 0.5
      } else {
        this.bullet.y += 0.5
      }

      if (this.down) { this.bullet.y += this.speed }
      if (this.up && this.bullet.y > this.y - 42) { this.bullet.y -= this.speed }

      if (this.bullet.y < this.y - 42) {
        this.bullet.bobbingUp = false
      } else if (this.bullet.y > this.y - 36) {
        this.bullet.bobbingUp = true
      }
    }
    if (this.left) { this.x -= this.speed }
    if (this.right) { this.x += this.speed }
    if (this.up) { this.y -= this.speed }
    if (this.down) { this.y += this.speed }
  }

  this.fireBullet = () => {
    if (!canFire) { return }
    canFire = false

    this.bullet = this.bullet ? this.bullet : bullets.add(this.x, this.y, 'water', 'held')
    this.bullet.state = 'fired'

    setTimeout(() => {
      canFire = true
    }, 600)

    this.bullet = null
  }

  this.holdBullet = (bullet) => {
    this.bullet = bullets.add(this.x + 14, this.y - 38, bullet.type, 'held')
    this.bullet.bobbingUp = false
  }

  this.fallForIt = (x, y, cb) => {
    moveToTarget.call(this, x, y, () => { this.left = true }, () => {
      cb()
      heartCloud()
      this.left = false
    }, 0.4 )

    this.partnered = true
    this.bullet && this.bullet.unassign()
    this.bullet = null
  }

  const heartCloud = () => {
    hearts = []
    let heartPairs = [
      {x: -30, y: -50},
      {x: 10, y: -70},
      {x: -10, y: -40},
      {x: 20, y: -30},
      {x: 35, y: -50},
    ]
    let heartWidth = global.arcadeMode ? 40 : 26
    heartPairs.forEach((pair, i) => {
      const heart = buildGameDiv({ className: 'greenHeart', src: 'assets/green.png', left: this.x + pair.x, top: this.y + pair.y, width: heartWidth, height:heartWidth })
      heart.x = pair.x
      heart.y = pair.y
      setTimeout(() => {
        heart.classList.add('greenHeartScaling')
      }, 200 * i)
      hearts.push(heart)
    })
  }

  bindPlayerInputs(this)
  return this
}
