import { buildGameDiv, position } from './_dom'

export default function Curtain(){

  const curtain = document.createElement('div')
  curtain.id = 'curtain'
  let curtainHeight = global.gameSize
  position(curtain, {top: global.height + global.padding, left: global.left + global.padding, height: curtainHeight, width: global.gameSize})
  document.body.appendChild(curtain)

  const metaMusic = document.getElementById('metaMusic')
  const beachMusic = document.getElementById('beachMusic')

  let playing = false
  let startScreen

  this.buildStartScreen = () => {
    startScreen = buildStartScreen(curtain)
    // startScreen = buildEndScreen(curtain) // test endscreen
    document.addEventListener('mousemove', (e) => {
      if (!playing) {
        metaMusic.play()
        playing = true
      }
    })
  }

  this.removeStartScreen = () => {
    curtain.removeChild(startScreen)
  }

  this.lowerCurtain = () => {
    curtain.style.opacity = 0.9
    let lowering = setInterval(() => {
      position(curtain, {top: global.height + global.padding, left: global.left + global.padding, height: curtainHeight, width: global.gameSize})
      curtainHeight += 10
      if (curtainHeight > global.gameSize) {
        clearInterval(lowering)
        metaMusic.play()
        beachMusic.pause()
        setTimeout(() => {
          buildEndScreen(curtain)
        }, 300)
        return
      }
    }, 5)
  }


  this.raiseCurtain = (cb) => {
    let raising = setInterval(() => {
      while (curtain.firstChild) {
        curtain.removeChild(curtain.firstChild)
      }
      position(curtain, {top: global.height + global.padding, left: global.left + global.padding, height: curtainHeight, width: global.gameSize})
      curtainHeight -= 10
      if (curtainHeight < 0) {
        clearInterval(raising)
        curtain.style.opacity = 0
        metaMusic.pause()
        beachMusic.play()
        cb()
        return
      }
    }, 5)
  }

  return this
}

function buildStartScreen (curtain){
  const el = buildGameDiv({parent: curtain })
  el.id = 'startScreen'

  const title = document.createElement('div')
  title.id = 'title'
  if (!global.arcadeMode) { title.classList.add('webMode') }
  const line1 = document.createElement('p')
  if (!global.arcadeMode) { line1.classList.add('webMode') }
  line1.innerHTML = 'a day at the'
  title.appendChild(line1)

  const line2 = document.createElement('p')
  line2.classList.add('line2')
  if (!global.arcadeMode) { line2.classList.add('webMode') }
  'beach'.split('').forEach((l, i) => {
    let letter = document.createElement('p')
    letter.innerHTML = l
    if (!global.arcadeMode) { letter.classList.add('webMode') }
    line2.appendChild(letter)
    setTimeout(() => {
      letter.classList.add('letterBouncing')
    }, i * 200)
  })
  line2.firstChild.classList.add('letterBouncing')

  title.appendChild(line2)

  if (global.arcadeMode) {
    const joystickText = document.createElement('p')
    joystickText.className += 'instruction'
    joystickText.innerHTML = 'joystick to MOVE'

    const largeBtnText = document.createElement('p')
    largeBtnText.className += 'instruction'
    largeBtnText.innerHTML = 'big button to FIRE'

    el.appendChild(title)
    el.appendChild(joystickText)
    el.appendChild(largeBtnText)

    const joystick = document.createElement('img')
    joystick.src = 'assets/joystick.png'
    joystick.id = 'joystick'
    curtain.appendChild(joystick)

    let counter = 0
    let minRotation = -4
    let maxRotation = 4
    let turningRight = true
    let joystickAnim = setInterval(() => {
      if (counter > maxRotation) {
        turningRight = false
      } else if (counter < minRotation) {
        turningRight = true
      }

      if (turningRight) {
        counter += 1
      } else {
        counter -= 1
      }

      joystick.style.transform = `rotate(${counter}deg)`
    }, 30)

    const restartBtn = document.createElement('img')
    restartBtn.src = 'assets/radio-1.png'
    restartBtn.id = 'restartBtn'
    curtain.appendChild(restartBtn)

    let restartBtnScale = 1
    setInterval(() => {
      if (restartBtnScale < 1) {
        restartBtnScale += 0.08
      } else {
        restartBtnScale -= 0.08
      }
      restartBtn.style.transform = `scale(${restartBtnScale}, ${restartBtnScale})`
    }, 400)
  } else {

    const arrows = document.createElement('p')
    arrows.className += 'instruction webMode'
    arrows.innerHTML = 'arrows/wasd to MOVE'

    const spacebar = document.createElement('p')
    spacebar.className += 'instruction webMode'
    spacebar.innerHTML = 'spacebar to FIRE'

    el.appendChild(title)
    el.appendChild(arrows)
    el.appendChild(spacebar)
  }

  curtain.appendChild(el)
  return el
}


function buildEndScreen (curtain){
  const el = document.createElement('div')
  el.id = 'endCredits'
  position(el, { top: global.height + 70, left: 20 })
  curtain.style.opacity = 0.96

  const designDev = document.createElement('p')
  designDev.className += 'role'
  if (!global.arcadeMode) { designDev.classList.add('webMode') }
  designDev.innerHTML = 'design & dev'
  const nicole = document.createElement('p')
  nicole.className += 'name'
  if (!global.arcadeMode) { nicole.classList.add('webMode') }
  nicole.innerHTML = 'nicole leffel'
  el.appendChild(designDev)
  el.appendChild(nicole)

  const music = document.createElement('p')
  music.className += 'role'
  if (!global.arcadeMode) { music.classList.add('webMode') }
  music.innerHTML = 'tunes'
  const visager = document.createElement('p')
  visager.className += 'name'
  if (!global.arcadeMode) { visager.classList.add('webMode') }
  visager.innerHTML = '@visager'
  music.style.color = '#ef83e4'
  el.appendChild(music)
  el.appendChild(visager)

  const backgroundArt = document.createElement('p')
  backgroundArt.className += 'role'
  if (!global.arcadeMode) { backgroundArt.classList.add('webMode') }
  backgroundArt.innerHTML = 'background'
  const bailey = document.createElement('p')
  bailey.className += 'name'
  if (!global.arcadeMode) { bailey.classList.add('webMode') }
  bailey.innerHTML = 'bailey steele'
  backgroundArt.style.color = '#3befd7'
  el.appendChild(backgroundArt)
  el.appendChild(bailey)

  let eagle
  let eagleEffect
  let player

  if (global.arcadeMode) {
    eagle = buildGameDiv({ id: 'finalEagle', src: 'assets/eagle.png', width: 74, height: 74, }) // del me
    eagleEffect = buildGameDiv({ parent: eagle, id: 'finalEagleEffect', src: 'assets/2hearts.png', width: 30, height: 30 })
    player = buildGameDiv({ id: 'player', src: 'assets/squid.png', width: 68, height: 68 })
    position(eagle, { left: global.left + 420, top: global.height + 140 })
    position(player, { left: global.left + 300, top: global.height + 143 })
    position(eagleEffect, { top: -126, left: 9, width: 40, height: 40 })
  } else {
    eagle = buildGameDiv({ id: 'finalEagle', src: 'assets/eagle.png', width: 64, height: 64, }) // del me
    eagleEffect = buildGameDiv({ parent: eagle, id: 'finalEagleEffect', src: 'assets/2hearts.png', width: 30, height: 30 })
    player = buildGameDiv({ id: 'player', src: 'assets/squid.png', width: 54, height: 54 })
    position(eagle, { left: global.left + 300, top: global.height + 95 })
    position(player, { left: global.left + 200, top: global.height + 108 })
    position(eagleEffect, { top: -106, left: 9, width: 30, height: 30 })
  }

  eagle.style.zIndex = 5
  eagleEffect.style.zIndex = 5
  player.style.zIndex = 6
  eagle.classList.add('finalEagleBobbing')
  eagleEffect.classList.add('bigPinkHeartPulse')
  player.classList.add('playerWobbleEnd')

  let heartPairs = [
    {x: -30, y: -50},
    {x: 10, y: -70},
    {x: -10, y: -34},
    {x: 20, y: -26},
    {x: 35, y: -50},
  ]
  heartPairs.forEach((pair, i) => {
    let heart
    if (global.arcadeMode) {
      heart = buildGameDiv({ className: 'greenHeart', src: 'assets/green.png', width: 30, height:30 })
      position(heart, { left: global.left + 310 + pair.x, top: global.height + 132 + pair.y })
    } else {
      heart = buildGameDiv({ className: 'greenHeart', src: 'assets/green.png', width: 26, height:26 })
      position(heart, { left: global.left + 206 + pair.x, top: global.height + 100 + pair.y })
    }

    if (i === 0) {
      heart.classList.add('greenHeartScalingEnd')
    } else {
      setTimeout(() => {
        heart.classList.add('greenHeartScalingEnd')
      }, 200 * i)
    }
  })

  document.body.appendChild(el)
  curtain.appendChild(el)
  return el
}
