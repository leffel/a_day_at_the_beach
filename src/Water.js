import { buildGameDiv } from './_dom'

export default function Water(bullets){
  this.x = global.left + global.padding
  this.y = global.height + (global.arcadeMode ? 580 : 412)
  this.width = global.gameSize
  this.height = 5
  this.tide = true
  this.shoreline = { x: this.x, y: this.y - 30, width: this.width, height: this.height }
  this.hitbox = { x: this.x, y: this.y - 35, width: this.width, height: this.height }

  let speed = 1.5
  let tideComingIn = true
  let maxHeight = 120
  let timeBetweenWaves = 3000

  this.bullets = []
  this.isSplashable = false

  const water = buildGameDiv({id: 'water', width: this.width, top: this.y, left: this.x})
  water.style.minHeight = this.height

  this.update = () => {
    if (this.tide){
      if (tideComingIn) {
        this.height += speed
        this.hitbox.height += speed * 0.6
      } else {
        this.height -= speed
        this.hitbox.height -= speed * 0.6
      }

      this.bullets.forEach(bullet => {
        if (!tideComingIn) {
          bullet.y -= speed
        }
        if (bullet.y < this.y - 20) {
          bullet.floatAway()
        }
      })


      if (this.height >= maxHeight) {
        tideComingIn = false
      } else if (this.height < 5) {
        tideComingIn = true
        this.bullets.filter(bullet => { return !bullet.cya })
        this.tide = false
        setTimeout(() => {
          this.tide = true
        }, timeBetweenWaves)
      }
    }
  }

  this.draw = () => {
    water.style.minHeight = `${this.height}px`
  }


  return this
}
