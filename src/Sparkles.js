import { teardown, buildGameDiv } from './_dom.js'

export default function Sparkles(){
  let rangeX
  let rangeY
  if (global.arcadeMode) {
    rangeX = { start: global.left + 340, width: 220 }
    rangeY = { start: global.height + 520, height: 30 }
  } else {
    rangeX = { start: global.left + 240, width: 180 }
    rangeY = { start: global.height + 364, height: 26 }
  }
  const limit = 7

  this.sparkles = 0

  this.update = () => {
    if (this.sparkles < limit) {
      let x = (Math.random() * rangeX.width) + rangeX.start
      let y = (Math.random() * rangeY.height) + rangeY.start
      new sparkle(x, y, this)
      this.sparkles++
    }
  }

  const _shakeDown = () => set.filter(function(sparkle){
    return !sparkle.cya
  })

  return this
}

function sparkle(x, y, sparkles){
  this.x = x
  this.y = y
  this.width = global.arcadeMode ? 5 : 4
  this.height = global.arcadeMode ? 5 : 4

  const lifespan = 4000
  const sparkle = buildGameDiv({ className: 'sparkle', top: this.y, left: this.x, width: this.width, })
  sparkle.style.minHeight = `${this.height}px`

  setTimeout(() => {
    teardown.call(this, sparkle)
    sparkles.sparkles--
  }, lifespan)
}
