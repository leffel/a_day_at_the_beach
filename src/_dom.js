import { pulse } from './_animations'

module.exports = {
  position: function(el, { left = null, top = null, width = null, height = null } = {}) {
    if (left) { el.style.left = `${left}px` }
    if (top) { el.style.top = `${top}px` }
    if (width) { el.style.width = `${width}px` }
    if (height) { el.style.height = `${height}px` }
  },

  teardown: function(el) {
    if (!this.cya) {
      while (el.firstChild) {
        el.removeChild(el.firstChild)
      }
      document.body.removeChild(el)
      this.update = function(){}
      this.draw = function(){}
    }
    this.cya = true
  },

  buildGameDiv: function({
      parent = null,
      id = null,
      className = null,
      src = null,
      left = null,
      top = null,
      width = null,
      height = null
    } = {}) {

    const el = document.createElement('div')
    el.className += 'gameObjDiv '

    if (id) { el.id = id }
    if (className) { el.className += className }
    if (src) {
      let img = document.createElement('img')
      img.className += 'gameObjImg'
      img.src = src
      el.appendChild(img)
    }

    module.exports.position(el, { left: left, top: top, width: width, height: height, })

    if (parent) {
      el.style.position = 'relative'
      parent.appendChild(el)
    } else {
      el.style.position = 'absolute'
      document.body.appendChild(el)
    }

    return el
  },
}
