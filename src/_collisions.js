import { teardown } from './_dom'

const _areColliding = (obj1, obj2) => {
  if (!obj1.cya && !obj2.cya &&
     obj1.x < obj2.x + obj2.width &&
     obj1.x + obj1.width > obj2.x &&
     obj1.y < obj2.y + obj2.height &&
     obj1.height + obj1.y > obj2.y) {
      return true
  }
}

module.exports = {
  playerEdge: (player) => {
    let paddedWidth = global.width - global.padding - 2
    let paddedHeight = global.height + global.gameSize - global.padding

    if (player.right && player.x + player.width > paddedWidth) {
      player.x -= player.speed
    }
    if (player.down && player.y + player.height > paddedHeight) {
      player.y -= player.speed
    }
  },
  playerShell: (player, shell, status) => {
    if (player.left && (_areColliding(player, shell) ||
      player.x < shell.x + shell.width - 10)) {
      player.x += player.speed
      shell.contact()
    }
  },
  playerWater: (player, water, status) => {
    if (player.up && _areColliding(player, water.shoreline) && global.round < 5) {
      player.y += player.speed
    }
    if (water.isSplashable && _areColliding(player, water.hitbox) && global.round > 0 && global.round < 3) {
      status.setMessage('splashin\' time', 'assets/wave.png')
      player.canSplash = true
    } else if (water.isSplashable && global.round > 0 && player.canSplash) {
      player.canSplash = false
      status.startStatus()
    }
  },
  bulletsErrbody: (bullets, player, water, flock) => {
    bullets.all.forEach(bullet => {
      if (!player.bullet && bullet.y > water.shoreline.y && !player.partnered && _areColliding(player, bullet)){
        player.holdBullet(bullet)
        bullet.unassign()
      }
      if (bullet.state === 'landed' && _areColliding(bullet, water)) {
        bullet.state = 'wet'
        water.bullets.push(bullet)
      }
      if (flock){
        flock.eagles.forEach(eagle => {
          if (_areColliding(eagle, bullet) && !eagle.pacified) {
            eagle.pacify(bullet.type)
            bullet.unassign()
            player.bullet = null
          }
        })
        if (flock.finalEagle && !flock.finalEagle.locked && _areColliding(flock.finalEagle, bullet)){
          flock.finalEagle.struck()
          player.bullet = null
        }
      }
    })
  }
}
