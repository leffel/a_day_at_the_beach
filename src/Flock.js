import { teardown, buildGameDiv, position } from "./_dom";
import { moveToTarget } from './_animations'

export default function Flock(player) {
  let movingLeft = true
  this.finalEagle = null
  // this.finalEagle = new final(global.left + 250, global.height + 200, movingLeft, player) // del me
  this.eagles = []

  this.update = () => {
    let stillSweeping = this.eagles.filter(eagle => { return !eagle.pacified })
    if (stillSweeping.length > 0) {
      _setEnds(stillSweeping)
      if (first.y < global.height + (global.arcadeMode ? 140 : 65)) {
        stillSweeping.forEach(eagle => (eagle.y += 6))
        return
      }
      if (movingLeft && first.x <= global.left + global.padding) {
        movingLeft = false
      } else if (!movingLeft && last.x + last.width >= global.width - 10) {
        movingLeft = true
      }
    }

    this.eagles.forEach(eagle => eagle.update(movingLeft))
    this.finalEagle && this.finalEagle.update(movingLeft)

    if (stillSweeping.length === 1 && global.round === 4) {
      let _finalEagle = stillSweeping[0]
      this.finalEagle = new final(_finalEagle.x, _finalEagle.y, movingLeft, player)
      _finalEagle.pacified = true
      _finalEagle.unassign()
    }
  }

  this.draw = () => {
    this.eagles.forEach(eagle => eagle.draw())
    this.finalEagle && this.finalEagle.draw()
  }

  this.callNextFlock = () => {
    let remaining = this.eagles.filter(eagle => { return !eagle.cya })
    return remaining.length > 0 ? false : true
  }

  const _init = () => {
    const rows = 2
    const per_row = 3
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < per_row; j++) {
        let y = i * 100 + (j % 2) * 80 - 60
        let x
        if (global.arcadeMode) {
          x = 230 + j * 130
        } else {
          x = 130 + j * 110
        }
        this.eagles.push(new eagle(x, y)) // make sure i'm on, disable for FE
      }
    }
  };

  let first = {}
  let last = {}
  const _setEnds = (remaining) => {
    first = remaining.sort((a, b) => {
      return a.x - b.x;
    })[0]
    last = remaining.sort((a, b) => {
      return b.x - a.x;
    })[0]
  };

  _init()

  return this;
}

function eagle(x, y) {
  this.x = x + global.left
  this.y = y
  this.width = global.arcadeMode ? 55 : 45
  this.height = global.arcadeMode ? 55 : 45
  this.speed = 3
  this.pacified = false
  this.animating = false

  const eagle = buildGameDiv({ src: 'assets/eagle.png', left: this.x, top: this.y, width: this.width, height: this.height })

  this.draw = () => {
    position(eagle, { top: this.y, left: this.x })
  };

  this.update = (movingLeft = true) => { movingLeft ? this.x -= this.speed : this.x += this.speed }

  this.pacify = function(bulletType){
    this.pacified = true

    let effect
    if (bulletType === 'water') {
      effect = buildGameDiv({ parent: eagle, src: 'assets/swirl.png', width: 40, height: 40 })
    } else {
      effect = buildGameDiv({ parent: eagle, src: 'assets/2hearts.png', width: 40, height: 40 })
    }

    this.draw = () => {
      position(eagle, { top: this.y, left: this.x })
      position(effect, { top: -90 })
      if (bulletType === 'water'){
        effect.classList.add('swirlRotate')
      } else {
        effect.classList.add('pinkHeartPulse')
      }
    }

    this.update = () => {
      this.y = this.y - this.speed
      if (this.y + this.height < 0) { this.unassign() }
    }
  }

  this.unassign = () => { teardown.call(this, eagle) }

  return this
}

function final(x, y, _movingLeft, player){
  this.x = x
  this.y = y
  this.width = global.arcadeMode ? 55 : 45
  this.height = global.arcadeMode ? 55 : 45
  this.speed = 3

  let movingLeft = _movingLeft
  this.landed = false
  this.locked = false

  let bobbingUp = true
  let minY
  let maxY

  const eagle = buildGameDiv({ id: 'finalEagle', src: 'assets/eagle.png', width: this.width, height: this.height, })

  this.animating = false
  this.draw = () => {
    position(eagle, { top: this.y, left: this.x })
  }

  this.update = () => {
    sweep()
    if (this.y > global.height + (global.arcadeMode ? 450 : 300)) {
      if (!this.locked) {
        moveToTarget.call(this, global.left + (global.arcadeMode ? 350 : 250), (global.arcadeMode ? 250 : 250), () => {}, () => {
          eagle.classList.add('finalEagleBobbing')
          player.fallForIt(global.left + (global.arcadeMode ? 350 : 250), global.height + (global.arcadeMode ? 600 : 400), () => {
            yerComingWithMe()
            console.log('am i stuck?')
            console.log(player)
          })
        })
      }
      this.locked = true
    }
  }

  const sweep = () => {
    if (movingLeft && this.x <= global.left + global.padding) {
      movingLeft = false
    } else if (!movingLeft && this.x + this.width >= global.width - global.padding) {
      movingLeft = true
    }
    if (movingLeft) {
      this.x -= this.speed
    } else {
      this.x += this.speed
    }
  }

  const yerComingWithMe = () => {
    setTimeout(() => {
      eagle.classList.remove('finalEagleBobbing')
      moveToTarget.call(this, player.x + 14, player.y - 20, () => {}, () => {
        this.update = () => {
          player.y -= this.speed * 2
          this.y = player.y - 20
          if (player.y + player.height < 0) {
            this.cya = true
          }
        }
      })
    }, 4000)
  }

  let effect = null
  this.struck = () => {
    if (!effect) {
      effect = buildGameDiv({ parent: eagle, id: 'finalEagleEffect', src: 'assets/2hearts.png', top: -86, left: 10, width: 30, height: 30 })
      effect.classList.add('bigPinkHeartPulse')
    } else {
      this.y += (global.arcadeMode ? 20 : 10)
    }
  }

  return this
}
