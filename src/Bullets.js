import { teardown, buildGameDiv, position } from './_dom.js'

export default function Bullets(status){
  this.all = []
  this.add = (x, y, typeName, state) => {
    let bullet = new Bullet(x, y, types[typeName], state, status)
    this.all.push(bullet)
    return bullet
  }
  this.update = () => {
    this.all.forEach(bullet => {
      bullet.update()
    })
  }
  this.draw = () => {
    this.all.forEach(bullet => {
      bullet.draw()
    })
  }
}

const Bullet = function(x, y, typeInfo, state, status){
  this.x = x
  this.y = y
  this.width = typeInfo.width
  this.height = typeInfo.height
  this.speed = 7
  this.type = typeInfo.type

  this.state = state

  let sineInc = 0.5
  let { amp, period, yOffset } = _setSine.call(this)

  const bullet = buildGameDiv({ className: typeInfo.className, src: typeInfo.img, left: x, top: y, width: typeInfo.width, height: typeInfo.height })
  status.setMessage(typeInfo.status, typeInfo.img)

  this.draw = () => {
    position(bullet, { top: this.y, left: this.x })
  }

  this.update = () => {
    switch(this.state) {
      case 'tossed':
        toss()
        break
      case 'landed':
        break
      case 'wet':
        break
      case 'held':
        break
      case 'fired':
        fire()
        break
      default:
        this.unassign()
    }
  }

  const fire = () => {
    this.y -= this.speed
    if (this.y + this.height < 0) {
      this.unassign() }
  }

  const toss = () => {
    if (this.state !== 'landed') {
      let slow = this.y < yOffset * 0.5
      this.y -= amp * Math.sin(sineInc) - 1
      this.x += slow ? 5 : 8
      sineInc += period
      if (this.y > yOffset) {
        this.state = 'landed'
      }
    }
  }

  this.unassign = () => {
    teardown.call(this, bullet)
  }

  this.floatAway = () => {
    bullet.classList.add('bulletFloatAway')
    setTimeout(() => {
      this.unassign()
    }, 1000)
  }

  return this
}

const _setSine = function() {
  /* A sin function is in the form of y = a * sin(b*x) + c,
  where c is the y-midpoint of the function (or the horizontal
  line across which the function oscillates), where a is the
  amplitude (maximal y-offset from y = c) and b is the period
  (number of x = 2*pi segments per wave). */

  let amp
  let period
  let yOffset
  if (global.arcadeMode) {
    amp = Math.random() * 9 + 5
    period = Math.PI * (Math.random() * (0.1 - 0.02) + 0.02)
    yOffset = this.y + (Math.random() * 10)
  } else {
    amp = Math.random() * 6 + 5
    period = Math.PI * (Math.random() * (0.1 - 0.04) + 0.04)
    yOffset = this.y + (Math.random() * 2)
  }

  return { amp: amp, period: period, yOffset: yOffset }
}

const types = {
  water: {
    type: 'water',
    img: 'assets/splish.png',
    width: 25,
    height: 25,
    status: 'warm and wet',
    className: 'waterBlt',
  },
  words: {
    type: 'words',
    img: 'assets/words.png',
    width: global.arcadeMode ? 40 : 34,
    height: global.arcadeMode ? 40 : 34,
    status: 'thoughtful words',
    className: null,
  },
  drama: {
    type: 'drama',
    img: 'assets/drama.png',
    width: global.arcadeMode ? 50 : 40,
    height: global.arcadeMode ? 50 : 40,
    status: 'ups & downs',
    className: null,
  },
  cherries: {
    type: 'cherries',
    img: 'assets/cherries.png',
    width: global.arcadeMode ? 45 : 35,
    height: global.arcadeMode ? 45 : 35,
    status: 'something sweet',
    className: null,
  },
  luck: {
    type: 'luck',
    img: 'assets/luck.png',
    width: global.arcadeMode ? 45 : 35,
    height: global.arcadeMode ? 45 : 35,
    status: 'pinch of luck',
    className: null,
  },
  music: {
    type: 'music',
    img: 'assets/music.png',
    width: global.arcadeMode ? 45 : 35,
    height: global.arcadeMode ? 45 : 35,
    status: 'the right tunes',
    className: null,
  },
  rainbow: {
    type: 'rainbow',
    img: 'assets/rainbow.png',
    width: global.arcadeMode ? 40 : 34,
    height: global.arcadeMode ? 40 : 34,
    status: 'radical optimism',
    className: null,
  },
  sunset: {
    type: 'sunset',
    img: 'assets/sunset.png',
    width: global.arcadeMode ? 40 : 34,
    height: global.arcadeMode ? 40 : 34,
    status: 'nostalgia for this moment',
    className: null,
  }
}
