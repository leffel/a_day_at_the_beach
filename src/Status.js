import { buildGameDiv } from './_dom.js'

export default function Status(){
  this.top = global.height + global.gameSize + (global.arcadeMode ? 70 : 26)
  this.left = global.left
  this.width = global.gameSize
  this.height = 40
  let updating = false

  const defaultMsg = {message: 'what a beautiful day', src: 'assets/sun.png'}
  const status = buildGameDiv({ id: 'status', top: this.top, left: this.left, width: this.width, height: this.height })
  if (!global.arcadeMode) { status.classList.add('webMode') }

  let timer
  this.setMessage = (message, src) => {
    if (updating) {
      clearTimeout(timer)
      while (status.firstChild) {
        status.removeChild(status.firstChild)
      }
      let img1 = buildGameDiv({ parent: status, src: src, className: 'statusImg', left: (global.arcadeMode ? -35 : -25), })
      if (!global.arcadeMode) { img1.classList.add('webMode') }
      status.appendChild(document.createTextNode(message))
      let img2 = buildGameDiv({ parent: status, src: src, className: 'statusImg', left: (global.arcadeMode ? 35 : 25), })
      if (!global.arcadeMode) { img2.classList.add('webMode') }

      if (message !== defaultMsg) {
        timer = setTimeout(() => { this.setMessage(defaultMsg.message, defaultMsg.src) }, 2000)
      }
    }
  }

  this.startStatus = () => {
    updating = true
    this.setMessage(defaultMsg.message, defaultMsg.src)
  }

  this.setFinalMessage = () => {
    clearTimeout(timer)
    while (status.firstChild) {
      status.removeChild(status.firstChild)
    }
    status.appendChild(document.createTextNode('press FIRE to restart!'))
  }

  status.appendChild(document.createTextNode('press FIRE to start!'))

  return this
}
