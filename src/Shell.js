import { buildGameDiv, position } from './_dom'

export default function Shell(bullets, status) {
  this.x = global.left + (global.arcadeMode ? 40 : 26)
  this.y = global.height + (global.arcadeMode ? 600 : 430)
  this.width = global.arcadeMode ? 80 : 60
  this.height = global.arcadeMode ? 80 : 60

  let speaking = false
  this.animating = false

  const shell = buildGameDiv({ id: 'shell', src: 'assets/shell.png', left: this.x, top: this.y, width: this.width, height: this.height,})
  const speechBubble = buildGameDiv({ parent: shell, id: 'shellBubble'})
  if (!global.arcadeMode) { speechBubble.classList.add('webMode') }

  this.draw = () => {
    position(shell, { top: this.y, left: this.x })
    speechBubble.style.display = speaking ? 'block' : 'none'
  }

  this.contact = () => {
    shell.classList.add('shellBounce')
    setTimeout(() => { shell.classList.remove('shellBounce')}, 500)
    if (!speaking) { _say('...', 3000) }
    status.setMessage('shell\'s grumpy even on nice days...', 'assets/shell.png')
  }

  let writing
  this.writeWords = () => {
    writing = setInterval(() => {
      bullets.add(this.x + 30, this.y + 30, 'words', 'tossed')
    }, 4000)
  }
  this.stopWriting = () => {
    clearInterval(writing)
  }

  let brainstorming
  this.startBrainstorming = () => {
    brainstorming = setInterval(() => {
      const specials = ['drama', 'cherries', 'luck', 'music', 'rainbow', 'sunset']
      const i = Math.floor((Math.random() * specials.length))
      bullets.add(this.x + (global.arcadeMode ? 30 : 0), this.y + (global.arcadeMode ? 30 : -10), specials[i], 'tossed')
    }, 5000)
  }
  this.stopBrainstorming = () => {
    clearInterval(brainstorming)
  }

  this.finalWords = () => {
    speechBubble.style.zIndex = 5
    setTimeout(() => {
      _say(':3')
    }, 100)
  }

  const _say = (msg, timeout = null) => {
    if (timeout) {
      if (speaking === false) {
        speaking = true
        speechBubble.innerHTML = msg
        setTimeout(() => {
          speaking = false
        }, timeout || 3000)
      }
    }
    speaking = true
    speechBubble.innerHTML = msg
  }

  this.eagles1Msg = (water) => {
    _say('ugh, them.', 3000)
    setTimeout(() => {
      _say('when the tide\'s in, <br/>splash \'em away!', 4000)
      water.isSplashable = true
    }, 3500)
  }

  this.eagles2Msg = (water,player) => {
    _say('again? <br/> so soon?!', 3000)
    setTimeout(() => {
      _say('okay, you know what to do.', 3000)
      water.isSplashable = true
      player.canSplash = true
    }, 3500)
  }

  this.eagles3Msg = (water) => {
    water.isSplashable = false
    _say('alright, kid. <br/> quit splashin\'.', 2500)
    setTimeout(() => {
      _say('we need a new strategy...', 2500)
      setTimeout(() => {
        _say('try this.', 2500)
        this.writeWords()
      }, 3000)
    }, 3000)
  }

  this.eagles4Msg = () => {
    _say('no way-', 2500)
    setTimeout(() => {
      _say('i mean no f*$^ing way!', 2500)
      setTimeout(() => {
        _say('...', 2500)
        setTimeout(() => {
          _say('hm.', 2500)
          setTimeout(() => {
            _say('maybe... ?', 3000)
            setTimeout(() => {
              this.startBrainstorming()
            }, 2500)
          }, 3000)
        }, 3000)
      }, 3000)
    }, 3000)
  }

  this.finaleEageMsg = () => {
    _say('!!!', 7000)
  }

  this.endCreditMsg = () => {
    shell.style.zIndex = 6
    speechBubble.style.zIndex = 6
    setTimeout(() => {
      speaking = true
      speechBubble.innerHTML = ':3'
    }, 2500)
  }

  return this
}
