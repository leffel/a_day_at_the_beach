module.exports = {
  moveToTarget: function(x, y, fn = null, cb = null, speedMod = 1) {
    this.update = () => {
      fn()
      let stopX = false
      if (this.x < x - 2) {
        this.x += this.speed * speedMod
      } else if (this.x > x + 2) {
        this.x -= this.speed * speedMod
      } else {
        stopX = true
      }

      let stopY = false
      if (this.y < y - 2) {
        this.y += this.speed * speedMod
      } else if (this.y > y + 2) {
        this.y -= this.speed * speedMod
      } else {
        stopY = true
      }

      if (stopX && stopY) {
        this.update = () => {}
        cb()
      }
    }
  },
  pulse: function(obj) {
    if (!this.animating){
      this.animating = true
      let scale = 0.8
      let animationTimer = setInterval(() => {
        if (scale < 1.1) {
          scale += 0.1
        } else {
          scale -= 0.1
        }
        obj.style.transform = `scale(${scale}, ${scale})`
      }, 200)
      setTimeout(() =>{
        clearInterval(animationTimer)
        this.animating = false
      }, 600)
    }
  },
  wobble: function(obj, _rotation) {
    if (!this.animating){
      let rotation = _rotation
      obj.style.transform = `rotate(${rotation}deg)`
      let animationTimer = setInterval(() => {
        if (rotation < 0) {
          rotation += _rotation
        } else {
          rotation -= _rotation
        }
        obj.style.transform = `rotate(${rotation}deg)`
      }, 100)
      setTimeout(() =>{
        clearInterval(animationTimer)
        this.animating = false
      }, 300)
    }
    this.animating = true
  }
}
