(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function (global){
'use strict';

var _Game = require('./src/Game');

var _Game2 = _interopRequireDefault(_Game);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.onload = function () {
  global.arcadeMode = false;
  global.gameSize = global.arcadeMode ? 700 : 500;
  global.padding = global.arcadeMode ? 10 : 5;

  global.left = (window.innerWidth - global.gameSize) * 0.5;
  global.width = global.left + global.gameSize;
  global.height = (window.innerHeight - global.gameSize) * 0.3;

  var background = document.createElement("img");
  background.id = 'background';
  background.src = "assets/baileys_bg.png";
  background.style.width = global.gameSize + 'px';
  background.style.height = global.gameSize + 'px';
  background.style.padding = global.padding + 'px';

  var container = document.createElement("div");
  container.style.width = width - 10 + 'px';

  var anotherContainer = document.createElement("div");
  anotherContainer.id = 'anotherContainer';
  anotherContainer.style.left = global.left + 'px';
  anotherContainer.style.top = global.height + 'px';

  anotherContainer.appendChild(background);
  container.appendChild(anotherContainer);
  document.body.appendChild(container);

  // still need onload if this is loaded with css?
  background.onload = function () {
    new _Game2.default();
  };
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./src/Game":5}],2:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Bullets;

var _dom = require('./_dom.js');

function Bullets(status) {
  var _this = this;

  this.all = [];
  this.add = function (x, y, typeName, state) {
    var bullet = new Bullet(x, y, types[typeName], state, status);
    _this.all.push(bullet);
    return bullet;
  };
  this.update = function () {
    _this.all.forEach(function (bullet) {
      bullet.update();
    });
  };
  this.draw = function () {
    _this.all.forEach(function (bullet) {
      bullet.draw();
    });
  };
}

var Bullet = function Bullet(x, y, typeInfo, state, status) {
  var _this2 = this;

  this.x = x;
  this.y = y;
  this.width = typeInfo.width;
  this.height = typeInfo.height;
  this.speed = 7;
  this.type = typeInfo.type;

  this.state = state;

  var sineInc = 0.5;

  var _setSine$call = _setSine.call(this),
      amp = _setSine$call.amp,
      period = _setSine$call.period,
      yOffset = _setSine$call.yOffset;

  var bullet = (0, _dom.buildGameDiv)({ className: typeInfo.className, src: typeInfo.img, left: x, top: y, width: typeInfo.width, height: typeInfo.height });
  status.setMessage(typeInfo.status, typeInfo.img);

  this.draw = function () {
    (0, _dom.position)(bullet, { top: _this2.y, left: _this2.x });
  };

  this.update = function () {
    switch (_this2.state) {
      case 'tossed':
        toss();
        break;
      case 'landed':
        break;
      case 'wet':
        break;
      case 'held':
        break;
      case 'fired':
        fire();
        break;
      default:
        _this2.unassign();
    }
  };

  var fire = function fire() {
    _this2.y -= _this2.speed;
    if (_this2.y + _this2.height < 0) {
      _this2.unassign();
    }
  };

  var toss = function toss() {
    if (_this2.state !== 'landed') {
      var slow = _this2.y < yOffset * 0.5;
      _this2.y -= amp * Math.sin(sineInc) - 1;
      _this2.x += slow ? 5 : 8;
      sineInc += period;
      if (_this2.y > yOffset) {
        _this2.state = 'landed';
      }
    }
  };

  this.unassign = function () {
    _dom.teardown.call(_this2, bullet);
  };

  this.floatAway = function () {
    bullet.classList.add('bulletFloatAway');
    setTimeout(function () {
      _this2.unassign();
    }, 1000);
  };

  return this;
};

var _setSine = function _setSine() {
  /* A sin function is in the form of y = a * sin(b*x) + c,
  where c is the y-midpoint of the function (or the horizontal
  line across which the function oscillates), where a is the
  amplitude (maximal y-offset from y = c) and b is the period
  (number of x = 2*pi segments per wave). */

  var amp = void 0;
  var period = void 0;
  var yOffset = void 0;
  if (global.arcadeMode) {
    amp = Math.random() * 9 + 5;
    period = Math.PI * (Math.random() * (0.1 - 0.02) + 0.02);
    yOffset = this.y + Math.random() * 10;
  } else {
    amp = Math.random() * 6 + 5;
    period = Math.PI * (Math.random() * (0.1 - 0.04) + 0.04);
    yOffset = this.y + Math.random() * 2;
  }

  return { amp: amp, period: period, yOffset: yOffset };
};

var types = {
  water: {
    type: 'water',
    img: 'assets/splish.png',
    width: 25,
    height: 25,
    status: 'warm and wet',
    className: 'waterBlt'
  },
  words: {
    type: 'words',
    img: 'assets/words.png',
    width: global.arcadeMode ? 40 : 34,
    height: global.arcadeMode ? 40 : 34,
    status: 'thoughtful words',
    className: null
  },
  drama: {
    type: 'drama',
    img: 'assets/drama.png',
    width: global.arcadeMode ? 50 : 40,
    height: global.arcadeMode ? 50 : 40,
    status: 'ups & downs',
    className: null
  },
  cherries: {
    type: 'cherries',
    img: 'assets/cherries.png',
    width: global.arcadeMode ? 45 : 35,
    height: global.arcadeMode ? 45 : 35,
    status: 'something sweet',
    className: null
  },
  luck: {
    type: 'luck',
    img: 'assets/luck.png',
    width: global.arcadeMode ? 45 : 35,
    height: global.arcadeMode ? 45 : 35,
    status: 'pinch of luck',
    className: null
  },
  music: {
    type: 'music',
    img: 'assets/music.png',
    width: global.arcadeMode ? 45 : 35,
    height: global.arcadeMode ? 45 : 35,
    status: 'the right tunes',
    className: null
  },
  rainbow: {
    type: 'rainbow',
    img: 'assets/rainbow.png',
    width: global.arcadeMode ? 40 : 34,
    height: global.arcadeMode ? 40 : 34,
    status: 'radical optimism',
    className: null
  },
  sunset: {
    type: 'sunset',
    img: 'assets/sunset.png',
    width: global.arcadeMode ? 40 : 34,
    height: global.arcadeMode ? 40 : 34,
    status: 'nostalgia for this moment',
    className: null
  }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./_dom.js":13}],3:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Curtain;

var _dom = require('./_dom');

function Curtain() {

  var curtain = document.createElement('div');
  curtain.id = 'curtain';
  var curtainHeight = global.gameSize;
  (0, _dom.position)(curtain, { top: global.height + global.padding, left: global.left + global.padding, height: curtainHeight, width: global.gameSize });
  document.body.appendChild(curtain);

  var metaMusic = document.getElementById('metaMusic');
  var beachMusic = document.getElementById('beachMusic');

  var playing = false;
  var startScreen = void 0;

  this.buildStartScreen = function () {
    startScreen = buildStartScreen(curtain);
    // startScreen = buildEndScreen(curtain) // test endscreen
    document.addEventListener('mousemove', function (e) {
      if (!playing) {
        metaMusic.play();
        playing = true;
      }
    });
  };

  this.removeStartScreen = function () {
    curtain.removeChild(startScreen);
  };

  this.lowerCurtain = function () {
    curtain.style.opacity = 0.9;
    var lowering = setInterval(function () {
      (0, _dom.position)(curtain, { top: global.height + global.padding, left: global.left + global.padding, height: curtainHeight, width: global.gameSize });
      curtainHeight += 10;
      if (curtainHeight > global.gameSize) {
        clearInterval(lowering);
        metaMusic.play();
        beachMusic.pause();
        setTimeout(function () {
          buildEndScreen(curtain);
        }, 300);
        return;
      }
    }, 5);
  };

  this.raiseCurtain = function (cb) {
    var raising = setInterval(function () {
      while (curtain.firstChild) {
        curtain.removeChild(curtain.firstChild);
      }
      (0, _dom.position)(curtain, { top: global.height + global.padding, left: global.left + global.padding, height: curtainHeight, width: global.gameSize });
      curtainHeight -= 10;
      if (curtainHeight < 0) {
        clearInterval(raising);
        curtain.style.opacity = 0;
        metaMusic.pause();
        beachMusic.play();
        cb();
        return;
      }
    }, 5);
  };

  return this;
}

function buildStartScreen(curtain) {
  var el = (0, _dom.buildGameDiv)({ parent: curtain });
  el.id = 'startScreen';

  var title = document.createElement('div');
  title.id = 'title';
  if (!global.arcadeMode) {
    title.classList.add('webMode');
  }
  var line1 = document.createElement('p');
  if (!global.arcadeMode) {
    line1.classList.add('webMode');
  }
  line1.innerHTML = 'a day at the';
  title.appendChild(line1);

  var line2 = document.createElement('p');
  line2.classList.add('line2');
  if (!global.arcadeMode) {
    line2.classList.add('webMode');
  }
  'beach'.split('').forEach(function (l, i) {
    var letter = document.createElement('p');
    letter.innerHTML = l;
    if (!global.arcadeMode) {
      letter.classList.add('webMode');
    }
    line2.appendChild(letter);
    setTimeout(function () {
      letter.classList.add('letterBouncing');
    }, i * 200);
  });
  line2.firstChild.classList.add('letterBouncing');

  title.appendChild(line2);

  if (global.arcadeMode) {
    var joystickText = document.createElement('p');
    joystickText.className += 'instruction';
    joystickText.innerHTML = 'joystick to MOVE';

    var largeBtnText = document.createElement('p');
    largeBtnText.className += 'instruction';
    largeBtnText.innerHTML = 'big button to FIRE';

    el.appendChild(title);
    el.appendChild(joystickText);
    el.appendChild(largeBtnText);

    var joystick = document.createElement('img');
    joystick.src = 'assets/joystick.png';
    joystick.id = 'joystick';
    curtain.appendChild(joystick);

    var counter = 0;
    var minRotation = -4;
    var maxRotation = 4;
    var turningRight = true;
    var joystickAnim = setInterval(function () {
      if (counter > maxRotation) {
        turningRight = false;
      } else if (counter < minRotation) {
        turningRight = true;
      }

      if (turningRight) {
        counter += 1;
      } else {
        counter -= 1;
      }

      joystick.style.transform = 'rotate(' + counter + 'deg)';
    }, 30);

    var restartBtn = document.createElement('img');
    restartBtn.src = 'assets/radio-1.png';
    restartBtn.id = 'restartBtn';
    curtain.appendChild(restartBtn);

    var restartBtnScale = 1;
    setInterval(function () {
      if (restartBtnScale < 1) {
        restartBtnScale += 0.08;
      } else {
        restartBtnScale -= 0.08;
      }
      restartBtn.style.transform = 'scale(' + restartBtnScale + ', ' + restartBtnScale + ')';
    }, 400);
  } else {

    var arrows = document.createElement('p');
    arrows.className += 'instruction webMode';
    arrows.innerHTML = 'arrows/wasd to MOVE';

    var spacebar = document.createElement('p');
    spacebar.className += 'instruction webMode';
    spacebar.innerHTML = 'spacebar to FIRE';

    el.appendChild(title);
    el.appendChild(arrows);
    el.appendChild(spacebar);
  }

  curtain.appendChild(el);
  return el;
}

function buildEndScreen(curtain) {
  var el = document.createElement('div');
  el.id = 'endCredits';
  (0, _dom.position)(el, { top: global.height + 70, left: 20 });
  curtain.style.opacity = 0.96;

  var designDev = document.createElement('p');
  designDev.className += 'role';
  if (!global.arcadeMode) {
    designDev.classList.add('webMode');
  }
  designDev.innerHTML = 'design & dev';
  var nicole = document.createElement('p');
  nicole.className += 'name';
  if (!global.arcadeMode) {
    nicole.classList.add('webMode');
  }
  nicole.innerHTML = 'nicole leffel';
  el.appendChild(designDev);
  el.appendChild(nicole);

  var music = document.createElement('p');
  music.className += 'role';
  if (!global.arcadeMode) {
    music.classList.add('webMode');
  }
  music.innerHTML = 'tunes';
  var visager = document.createElement('p');
  visager.className += 'name';
  if (!global.arcadeMode) {
    visager.classList.add('webMode');
  }
  visager.innerHTML = '@visager';
  music.style.color = '#ef83e4';
  el.appendChild(music);
  el.appendChild(visager);

  var backgroundArt = document.createElement('p');
  backgroundArt.className += 'role';
  if (!global.arcadeMode) {
    backgroundArt.classList.add('webMode');
  }
  backgroundArt.innerHTML = 'background';
  var bailey = document.createElement('p');
  bailey.className += 'name';
  if (!global.arcadeMode) {
    bailey.classList.add('webMode');
  }
  bailey.innerHTML = 'bailey steele';
  backgroundArt.style.color = '#3befd7';
  el.appendChild(backgroundArt);
  el.appendChild(bailey);

  var eagle = void 0;
  var eagleEffect = void 0;
  var player = void 0;

  if (global.arcadeMode) {
    eagle = (0, _dom.buildGameDiv)({ id: 'finalEagle', src: 'assets/eagle.png', width: 74, height: 74 }); // del me
    eagleEffect = (0, _dom.buildGameDiv)({ parent: eagle, id: 'finalEagleEffect', src: 'assets/2hearts.png', width: 30, height: 30 });
    player = (0, _dom.buildGameDiv)({ id: 'player', src: 'assets/squid.png', width: 68, height: 68 });
    (0, _dom.position)(eagle, { left: global.left + 420, top: global.height + 140 });
    (0, _dom.position)(player, { left: global.left + 300, top: global.height + 143 });
    (0, _dom.position)(eagleEffect, { top: -126, left: 9, width: 40, height: 40 });
  } else {
    eagle = (0, _dom.buildGameDiv)({ id: 'finalEagle', src: 'assets/eagle.png', width: 64, height: 64 }); // del me
    eagleEffect = (0, _dom.buildGameDiv)({ parent: eagle, id: 'finalEagleEffect', src: 'assets/2hearts.png', width: 30, height: 30 });
    player = (0, _dom.buildGameDiv)({ id: 'player', src: 'assets/squid.png', width: 54, height: 54 });
    (0, _dom.position)(eagle, { left: global.left + 300, top: global.height + 95 });
    (0, _dom.position)(player, { left: global.left + 200, top: global.height + 108 });
    (0, _dom.position)(eagleEffect, { top: -106, left: 9, width: 30, height: 30 });
  }

  eagle.style.zIndex = 5;
  eagleEffect.style.zIndex = 5;
  player.style.zIndex = 6;
  eagle.classList.add('finalEagleBobbing');
  eagleEffect.classList.add('bigPinkHeartPulse');
  player.classList.add('playerWobbleEnd');

  var heartPairs = [{ x: -30, y: -50 }, { x: 10, y: -70 }, { x: -10, y: -34 }, { x: 20, y: -26 }, { x: 35, y: -50 }];
  heartPairs.forEach(function (pair, i) {
    var heart = void 0;
    if (global.arcadeMode) {
      heart = (0, _dom.buildGameDiv)({ className: 'greenHeart', src: 'assets/green.png', width: 30, height: 30 });
      (0, _dom.position)(heart, { left: global.left + 310 + pair.x, top: global.height + 132 + pair.y });
    } else {
      heart = (0, _dom.buildGameDiv)({ className: 'greenHeart', src: 'assets/green.png', width: 26, height: 26 });
      (0, _dom.position)(heart, { left: global.left + 206 + pair.x, top: global.height + 100 + pair.y });
    }

    if (i === 0) {
      heart.classList.add('greenHeartScalingEnd');
    } else {
      setTimeout(function () {
        heart.classList.add('greenHeartScalingEnd');
      }, 200 * i);
    }
  });

  document.body.appendChild(el);
  curtain.appendChild(el);
  return el;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./_dom":13}],4:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Flock;

var _dom = require('./_dom');

var _animations = require('./_animations');

function Flock(player) {
  var _this = this;

  var movingLeft = true;
  this.finalEagle = null;
  // this.finalEagle = new final(global.left + 250, global.height + 200, movingLeft, player) // del me
  this.eagles = [];

  this.update = function () {
    var stillSweeping = _this.eagles.filter(function (eagle) {
      return !eagle.pacified;
    });
    if (stillSweeping.length > 0) {
      _setEnds(stillSweeping);
      if (first.y < global.height + (global.arcadeMode ? 140 : 65)) {
        stillSweeping.forEach(function (eagle) {
          return eagle.y += 6;
        });
        return;
      }
      if (movingLeft && first.x <= global.left + global.padding) {
        movingLeft = false;
      } else if (!movingLeft && last.x + last.width >= global.width - 10) {
        movingLeft = true;
      }
    }

    _this.eagles.forEach(function (eagle) {
      return eagle.update(movingLeft);
    });
    _this.finalEagle && _this.finalEagle.update(movingLeft);

    if (stillSweeping.length === 1 && global.round === 4) {
      var _finalEagle = stillSweeping[0];
      _this.finalEagle = new final(_finalEagle.x, _finalEagle.y, movingLeft, player);
      _finalEagle.pacified = true;
      _finalEagle.unassign();
    }
  };

  this.draw = function () {
    _this.eagles.forEach(function (eagle) {
      return eagle.draw();
    });
    _this.finalEagle && _this.finalEagle.draw();
  };

  this.callNextFlock = function () {
    var remaining = _this.eagles.filter(function (eagle) {
      return !eagle.cya;
    });
    return remaining.length > 0 ? false : true;
  };

  var _init = function _init() {
    var rows = 2;
    var per_row = 3;
    for (var i = 0; i < rows; i++) {
      for (var j = 0; j < per_row; j++) {
        var y = i * 100 + j % 2 * 80 - 60;
        var x = void 0;
        if (global.arcadeMode) {
          x = 230 + j * 130;
        } else {
          x = 130 + j * 110;
        }
        _this.eagles.push(new eagle(x, y)); // make sure i'm on, disable for FE
      }
    }
  };

  var first = {};
  var last = {};
  var _setEnds = function _setEnds(remaining) {
    first = remaining.sort(function (a, b) {
      return a.x - b.x;
    })[0];
    last = remaining.sort(function (a, b) {
      return b.x - a.x;
    })[0];
  };

  _init();

  return this;
}

function eagle(x, y) {
  var _this2 = this;

  this.x = x + global.left;
  this.y = y;
  this.width = global.arcadeMode ? 55 : 45;
  this.height = global.arcadeMode ? 55 : 45;
  this.speed = 3;
  this.pacified = false;
  this.animating = false;

  var eagle = (0, _dom.buildGameDiv)({ src: 'assets/eagle.png', left: this.x, top: this.y, width: this.width, height: this.height });

  this.draw = function () {
    (0, _dom.position)(eagle, { top: _this2.y, left: _this2.x });
  };

  this.update = function () {
    var movingLeft = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
    movingLeft ? _this2.x -= _this2.speed : _this2.x += _this2.speed;
  };

  this.pacify = function (bulletType) {
    var _this3 = this;

    this.pacified = true;

    var effect = void 0;
    if (bulletType === 'water') {
      effect = (0, _dom.buildGameDiv)({ parent: eagle, src: 'assets/swirl.png', width: 40, height: 40 });
    } else {
      effect = (0, _dom.buildGameDiv)({ parent: eagle, src: 'assets/2hearts.png', width: 40, height: 40 });
    }

    this.draw = function () {
      (0, _dom.position)(eagle, { top: _this3.y, left: _this3.x });
      (0, _dom.position)(effect, { top: -90 });
      if (bulletType === 'water') {
        effect.classList.add('swirlRotate');
      } else {
        effect.classList.add('pinkHeartPulse');
      }
    };

    this.update = function () {
      _this3.y = _this3.y - _this3.speed;
      if (_this3.y + _this3.height < 0) {
        _this3.unassign();
      }
    };
  };

  this.unassign = function () {
    _dom.teardown.call(_this2, eagle);
  };

  return this;
}

function final(x, y, _movingLeft, player) {
  var _this4 = this;

  this.x = x;
  this.y = y;
  this.width = global.arcadeMode ? 55 : 45;
  this.height = global.arcadeMode ? 55 : 45;
  this.speed = 3;

  var movingLeft = _movingLeft;
  this.landed = false;
  this.locked = false;

  var bobbingUp = true;
  var minY = void 0;
  var maxY = void 0;

  var eagle = (0, _dom.buildGameDiv)({ id: 'finalEagle', src: 'assets/eagle.png', width: this.width, height: this.height });

  this.animating = false;
  this.draw = function () {
    (0, _dom.position)(eagle, { top: _this4.y, left: _this4.x });
  };

  this.update = function () {
    sweep();
    if (_this4.y > global.height + (global.arcadeMode ? 450 : 300)) {
      if (!_this4.locked) {
        _animations.moveToTarget.call(_this4, global.left + (global.arcadeMode ? 350 : 250), global.arcadeMode ? 250 : 250, function () {}, function () {
          eagle.classList.add('finalEagleBobbing');
          player.fallForIt(global.left + (global.arcadeMode ? 350 : 250), global.height + (global.arcadeMode ? 600 : 400), function () {
            yerComingWithMe();
            console.log('am i stuck?');
            console.log(player);
          });
        });
      }
      _this4.locked = true;
    }
  };

  var sweep = function sweep() {
    if (movingLeft && _this4.x <= global.left + global.padding) {
      movingLeft = false;
    } else if (!movingLeft && _this4.x + _this4.width >= global.width - global.padding) {
      movingLeft = true;
    }
    if (movingLeft) {
      _this4.x -= _this4.speed;
    } else {
      _this4.x += _this4.speed;
    }
  };

  var yerComingWithMe = function yerComingWithMe() {
    setTimeout(function () {
      eagle.classList.remove('finalEagleBobbing');
      _animations.moveToTarget.call(_this4, player.x + 14, player.y - 20, function () {}, function () {
        _this4.update = function () {
          player.y -= _this4.speed * 2;
          _this4.y = player.y - 20;
          if (player.y + player.height < 0) {
            _this4.cya = true;
          }
        };
      });
    }, 4000);
  };

  var effect = null;
  this.struck = function () {
    if (!effect) {
      effect = (0, _dom.buildGameDiv)({ parent: eagle, id: 'finalEagleEffect', src: 'assets/2hearts.png', top: -86, left: 10, width: 30, height: 30 });
      effect.classList.add('bigPinkHeartPulse');
    } else {
      _this4.y += global.arcadeMode ? 20 : 10;
    }
  };

  return this;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./_animations":11,"./_dom":13}],5:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Game;

var _Shell = require('./Shell');

var _Shell2 = _interopRequireDefault(_Shell);

var _Player = require('./Player');

var _Player2 = _interopRequireDefault(_Player);

var _Flock = require('./Flock');

var _Flock2 = _interopRequireDefault(_Flock);

var _Sparkles = require('./Sparkles');

var _Sparkles2 = _interopRequireDefault(_Sparkles);

var _Status = require('./Status');

var _Status2 = _interopRequireDefault(_Status);

var _Water = require('./Water');

var _Water2 = _interopRequireDefault(_Water);

var _Bullets = require('./Bullets');

var _Bullets2 = _interopRequireDefault(_Bullets);

var _Curtain = require('./Curtain');

var _Curtain2 = _interopRequireDefault(_Curtain);

var _collisions = require('./_collisions');

var _dom = require('./_dom');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Game() {
  global.round = 0;
  var fps = 30;
  var status = new _Status2.default();
  var bullets = new _Bullets2.default(status);
  var shell = new _Shell2.default(bullets, status);
  var player = new _Player2.default(bullets);
  var sparkles = new _Sparkles2.default();
  var water = new _Water2.default(bullets);
  var curtain = new _Curtain2.default();

  // let flock = new Flock(global.round, player) // start null!
  var flock = null;
  var omw = false;

  var _nextRound = function _nextRound() {
    global.round++;
    if (global.round === 1) {
      flock = new _Flock2.default(player);
      status.startStatus();
      setTimeout(function () {
        shell.eagles1Msg(water);
      }, 2000);
    }
    if (global.round === 2) {
      water.isSplashable = false;
      player.canSplash = false;
      setTimeout(function () {
        shell.eagles2Msg(water, player);
      }, 2000);
    }
    if (global.round === 3) {
      setTimeout(function () {
        shell.eagles3Msg(water);
      }, 2000);
      player.canSplash = false;
    }
    if (global.round === 4) {
      setTimeout(function () {
        shell.eagles4Msg();
      }, 2000);
      shell.stopWriting();
    }
    if (global.round === 5) {
      player.controllable = false;
      setTimeout(function () {
        shell.finaleEageMsg();
      }, 2000);
      shell.stopBrainstorming();
    }
    if (global.round === 6) {
      finalEagle.update = function () {};
      player.update = function () {};
      document.getElementById('shell').style.zIndex = 5;
      setTimeout(function () {
        curtain.lowerCurtain(flock.finalEagle, shell.endCreditMsg);
      }, 200);
      setTimeout(function () {
        shell.finalWords();
      }, 1200);
      status.setFinalMessage();
    }
    console.log('global.round is ' + global.round);
  };

  var _manageGame = function _manageGame() {
    if (!flock && !omw && global.round > 0) {
      omw = true;
      setTimeout(function () {
        _nextRound();
        flock = new _Flock2.default(player);
        omw = false;
      }, 2000);
    } else if (global.round < 4 && flock && flock.callNextFlock()) {
      flock = null;
    } else if (global.round === 4 && flock && flock.finalEagle && flock.finalEagle.locked) {
      _nextRound();
    } else if (global.round === 5 && flock && flock.finalEagle.cya) {
      _nextRound();
    }
  };

  var _collide = function _collide() {
    (0, _collisions.playerEdge)(player, global.width, global.height);
    (0, _collisions.playerShell)(player, shell, status);
    (0, _collisions.playerWater)(player, water, status);
    (0, _collisions.bulletsErrbody)(bullets, player, water, flock);
  };

  var _update = function _update() {
    player.update();
    flock && flock.update();

    sparkles.update();
    water.update();
    bullets.update();
  };

  var _draw = function _draw() {
    player.draw();
    flock && flock.draw();
    shell.draw();
    water.draw();
    bullets.draw();
  };

  setInterval(function () {
    _manageGame();
    _collide();
    _update();
    _draw();
  }, 1000 / fps);

  var started = false;
  var startGame = function startGame() {
    curtain.buildStartScreen();
    document.addEventListener('keydown', function (e) {
      if (e.keyCode === 32 && !started) {
        started = true;
        curtain.removeStartScreen();
        curtain.raiseCurtain(_nextRound);
      }
    });
  };

  startGame();
  // _nextRound() //turn me off
  return this;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./Bullets":2,"./Curtain":3,"./Flock":4,"./Player":6,"./Shell":7,"./Sparkles":8,"./Status":9,"./Water":10,"./_collisions":12,"./_dom":13}],6:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Player;

var _keybindings = require('./_keybindings');

var _dom = require('./_dom');

var _animations = require('./_animations');

function Player(bullets) {
  var _this = this;

  this.x = global.left + global.gameSize / 2;
  this.y = global.height + global.gameSize - 90;
  this.width = global.arcadeMode ? 55 : 45;
  this.height = global.arcadeMode ? 55 : 45;
  this.speed = global.arcadeMode ? 6 : 4;

  this.left = false;
  this.right = false;
  this.up = false;
  this.down = false;

  this.bullet = null;
  this.shotFired = false;
  this.canSplash = false;
  this.partnered = false;

  this.animating = false;
  var canFire = true;
  this.controllable = true;

  this.squid = (0, _dom.buildGameDiv)({
    id: 'player',
    src: 'assets/squid.png',
    left: this.x + 8,
    top: this.y + 8,
    width: this.width,
    height: this.height
  });

  var hearts = [];
  this.draw = function () {
    (0, _dom.position)(_this.squid, { top: _this.y + 8, left: _this.x + 8 });
    if (_this.left || _this.right || _this.up || _this.down) {
      _this.squid.classList.add('playerWobble');
    } else {
      _this.squid.classList.remove('playerWobble');
    }
    hearts.forEach(function (heart) {
      (0, _dom.position)(heart, { left: _this.x + heart.x + (global.arcadeMode ? 0 : 8), top: _this.y + heart.y + (global.arcadeMode ? 0 : 2) });
    });
  };

  this.update = function () {
    if (_this.bullet) {
      _this.bullet.x = _this.x + 14;
      if (_this.bullet.bobbingUp) {
        _this.bullet.y -= 0.5;
      } else {
        _this.bullet.y += 0.5;
      }

      if (_this.down) {
        _this.bullet.y += _this.speed;
      }
      if (_this.up && _this.bullet.y > _this.y - 42) {
        _this.bullet.y -= _this.speed;
      }

      if (_this.bullet.y < _this.y - 42) {
        _this.bullet.bobbingUp = false;
      } else if (_this.bullet.y > _this.y - 36) {
        _this.bullet.bobbingUp = true;
      }
    }
    if (_this.left) {
      _this.x -= _this.speed;
    }
    if (_this.right) {
      _this.x += _this.speed;
    }
    if (_this.up) {
      _this.y -= _this.speed;
    }
    if (_this.down) {
      _this.y += _this.speed;
    }
  };

  this.fireBullet = function () {
    if (!canFire) {
      return;
    }
    canFire = false;

    _this.bullet = _this.bullet ? _this.bullet : bullets.add(_this.x, _this.y, 'water', 'held');
    _this.bullet.state = 'fired';

    setTimeout(function () {
      canFire = true;
    }, 600);

    _this.bullet = null;
  };

  this.holdBullet = function (bullet) {
    _this.bullet = bullets.add(_this.x + 14, _this.y - 38, bullet.type, 'held');
    _this.bullet.bobbingUp = false;
  };

  this.fallForIt = function (x, y, cb) {
    _animations.moveToTarget.call(_this, x, y, function () {
      _this.left = true;
    }, function () {
      cb();
      heartCloud();
      _this.left = false;
    }, 0.4);

    _this.partnered = true;
    _this.bullet && _this.bullet.unassign();
    _this.bullet = null;
  };

  var heartCloud = function heartCloud() {
    hearts = [];
    var heartPairs = [{ x: -30, y: -50 }, { x: 10, y: -70 }, { x: -10, y: -40 }, { x: 20, y: -30 }, { x: 35, y: -50 }];
    var heartWidth = global.arcadeMode ? 40 : 26;
    heartPairs.forEach(function (pair, i) {
      var heart = (0, _dom.buildGameDiv)({ className: 'greenHeart', src: 'assets/green.png', left: _this.x + pair.x, top: _this.y + pair.y, width: heartWidth, height: heartWidth });
      heart.x = pair.x;
      heart.y = pair.y;
      setTimeout(function () {
        heart.classList.add('greenHeartScaling');
      }, 200 * i);
      hearts.push(heart);
    });
  };

  (0, _keybindings.bindPlayerInputs)(this);
  return this;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./_animations":11,"./_dom":13,"./_keybindings":14}],7:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Shell;

var _dom = require('./_dom');

function Shell(bullets, status) {
  var _this = this;

  this.x = global.left + (global.arcadeMode ? 40 : 26);
  this.y = global.height + (global.arcadeMode ? 600 : 430);
  this.width = global.arcadeMode ? 80 : 60;
  this.height = global.arcadeMode ? 80 : 60;

  var speaking = false;
  this.animating = false;

  var shell = (0, _dom.buildGameDiv)({ id: 'shell', src: 'assets/shell.png', left: this.x, top: this.y, width: this.width, height: this.height });
  var speechBubble = (0, _dom.buildGameDiv)({ parent: shell, id: 'shellBubble' });
  if (!global.arcadeMode) {
    speechBubble.classList.add('webMode');
  }

  this.draw = function () {
    (0, _dom.position)(shell, { top: _this.y, left: _this.x });
    speechBubble.style.display = speaking ? 'block' : 'none';
  };

  this.contact = function () {
    shell.classList.add('shellBounce');
    setTimeout(function () {
      shell.classList.remove('shellBounce');
    }, 500);
    if (!speaking) {
      _say('...', 3000);
    }
    status.setMessage('shell\'s grumpy even on nice days...', 'assets/shell.png');
  };

  var writing = void 0;
  this.writeWords = function () {
    writing = setInterval(function () {
      bullets.add(_this.x + 30, _this.y + 30, 'words', 'tossed');
    }, 4000);
  };
  this.stopWriting = function () {
    clearInterval(writing);
  };

  var brainstorming = void 0;
  this.startBrainstorming = function () {
    brainstorming = setInterval(function () {
      var specials = ['drama', 'cherries', 'luck', 'music', 'rainbow', 'sunset'];
      var i = Math.floor(Math.random() * specials.length);
      bullets.add(_this.x + (global.arcadeMode ? 30 : 0), _this.y + (global.arcadeMode ? 30 : -10), specials[i], 'tossed');
    }, 5000);
  };
  this.stopBrainstorming = function () {
    clearInterval(brainstorming);
  };

  this.finalWords = function () {
    speechBubble.style.zIndex = 5;
    setTimeout(function () {
      _say(':3');
    }, 100);
  };

  var _say = function _say(msg) {
    var timeout = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

    if (timeout) {
      if (speaking === false) {
        speaking = true;
        speechBubble.innerHTML = msg;
        setTimeout(function () {
          speaking = false;
        }, timeout || 3000);
      }
    }
    speaking = true;
    speechBubble.innerHTML = msg;
  };

  this.eagles1Msg = function (water) {
    _say('ugh, them.', 3000);
    setTimeout(function () {
      _say('when the tide\'s in, <br/>splash \'em away!', 4000);
      water.isSplashable = true;
    }, 3500);
  };

  this.eagles2Msg = function (water, player) {
    _say('again? <br/> so soon?!', 3000);
    setTimeout(function () {
      _say('okay, you know what to do.', 3000);
      water.isSplashable = true;
      player.canSplash = true;
    }, 3500);
  };

  this.eagles3Msg = function (water) {
    water.isSplashable = false;
    _say('alright, kid. <br/> quit splashin\'.', 2500);
    setTimeout(function () {
      _say('we need a new strategy...', 2500);
      setTimeout(function () {
        _say('try this.', 2500);
        _this.writeWords();
      }, 3000);
    }, 3000);
  };

  this.eagles4Msg = function () {
    _say('no way-', 2500);
    setTimeout(function () {
      _say('i mean no f*$^ing way!', 2500);
      setTimeout(function () {
        _say('...', 2500);
        setTimeout(function () {
          _say('hm.', 2500);
          setTimeout(function () {
            _say('maybe... ?', 3000);
            setTimeout(function () {
              _this.startBrainstorming();
            }, 2500);
          }, 3000);
        }, 3000);
      }, 3000);
    }, 3000);
  };

  this.finaleEageMsg = function () {
    _say('!!!', 7000);
  };

  this.endCreditMsg = function () {
    shell.style.zIndex = 6;
    speechBubble.style.zIndex = 6;
    setTimeout(function () {
      speaking = true;
      speechBubble.innerHTML = ':3';
    }, 2500);
  };

  return this;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./_dom":13}],8:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Sparkles;

var _dom = require('./_dom.js');

function Sparkles() {
  var _this = this;

  var rangeX = void 0;
  var rangeY = void 0;
  if (global.arcadeMode) {
    rangeX = { start: global.left + 340, width: 220 };
    rangeY = { start: global.height + 520, height: 30 };
  } else {
    rangeX = { start: global.left + 240, width: 180 };
    rangeY = { start: global.height + 364, height: 26 };
  }
  var limit = 7;

  this.sparkles = 0;

  this.update = function () {
    if (_this.sparkles < limit) {
      var x = Math.random() * rangeX.width + rangeX.start;
      var y = Math.random() * rangeY.height + rangeY.start;
      new sparkle(x, y, _this);
      _this.sparkles++;
    }
  };

  var _shakeDown = function _shakeDown() {
    return set.filter(function (sparkle) {
      return !sparkle.cya;
    });
  };

  return this;
}

function sparkle(x, y, sparkles) {
  var _this2 = this;

  this.x = x;
  this.y = y;
  this.width = global.arcadeMode ? 5 : 4;
  this.height = global.arcadeMode ? 5 : 4;

  var lifespan = 4000;
  var sparkle = (0, _dom.buildGameDiv)({ className: 'sparkle', top: this.y, left: this.x, width: this.width });
  sparkle.style.minHeight = this.height + 'px';

  setTimeout(function () {
    _dom.teardown.call(_this2, sparkle);
    sparkles.sparkles--;
  }, lifespan);
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./_dom.js":13}],9:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Status;

var _dom = require('./_dom.js');

function Status() {
  var _this = this;

  this.top = global.height + global.gameSize + (global.arcadeMode ? 70 : 26);
  this.left = global.left;
  this.width = global.gameSize;
  this.height = 40;
  var updating = false;

  var defaultMsg = { message: 'what a beautiful day', src: 'assets/sun.png' };
  var status = (0, _dom.buildGameDiv)({ id: 'status', top: this.top, left: this.left, width: this.width, height: this.height });
  if (!global.arcadeMode) {
    status.classList.add('webMode');
  }

  var timer = void 0;
  this.setMessage = function (message, src) {
    if (updating) {
      clearTimeout(timer);
      while (status.firstChild) {
        status.removeChild(status.firstChild);
      }
      var img1 = (0, _dom.buildGameDiv)({ parent: status, src: src, className: 'statusImg', left: global.arcadeMode ? -35 : -25 });
      if (!global.arcadeMode) {
        img1.classList.add('webMode');
      }
      status.appendChild(document.createTextNode(message));
      var img2 = (0, _dom.buildGameDiv)({ parent: status, src: src, className: 'statusImg', left: global.arcadeMode ? 35 : 25 });
      if (!global.arcadeMode) {
        img2.classList.add('webMode');
      }

      if (message !== defaultMsg) {
        timer = setTimeout(function () {
          _this.setMessage(defaultMsg.message, defaultMsg.src);
        }, 2000);
      }
    }
  };

  this.startStatus = function () {
    updating = true;
    _this.setMessage(defaultMsg.message, defaultMsg.src);
  };

  this.setFinalMessage = function () {
    clearTimeout(timer);
    while (status.firstChild) {
      status.removeChild(status.firstChild);
    }
    status.appendChild(document.createTextNode('press FIRE to restart!'));
  };

  status.appendChild(document.createTextNode('press FIRE to start!'));

  return this;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./_dom.js":13}],10:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Water;

var _dom = require('./_dom');

function Water(bullets) {
  var _this = this;

  this.x = global.left + global.padding;
  this.y = global.height + (global.arcadeMode ? 580 : 412);
  this.width = global.gameSize;
  this.height = 5;
  this.tide = true;
  this.shoreline = { x: this.x, y: this.y - 30, width: this.width, height: this.height };
  this.hitbox = { x: this.x, y: this.y - 35, width: this.width, height: this.height };

  var speed = 1.5;
  var tideComingIn = true;
  var maxHeight = 120;
  var timeBetweenWaves = 3000;

  this.bullets = [];
  this.isSplashable = false;

  var water = (0, _dom.buildGameDiv)({ id: 'water', width: this.width, top: this.y, left: this.x });
  water.style.minHeight = this.height;

  this.update = function () {
    if (_this.tide) {
      if (tideComingIn) {
        _this.height += speed;
        _this.hitbox.height += speed * 0.6;
      } else {
        _this.height -= speed;
        _this.hitbox.height -= speed * 0.6;
      }

      _this.bullets.forEach(function (bullet) {
        if (!tideComingIn) {
          bullet.y -= speed;
        }
        if (bullet.y < _this.y - 20) {
          bullet.floatAway();
        }
      });

      if (_this.height >= maxHeight) {
        tideComingIn = false;
      } else if (_this.height < 5) {
        tideComingIn = true;
        _this.bullets.filter(function (bullet) {
          return !bullet.cya;
        });
        _this.tide = false;
        setTimeout(function () {
          _this.tide = true;
        }, timeBetweenWaves);
      }
    }
  };

  this.draw = function () {
    water.style.minHeight = _this.height + 'px';
  };

  return this;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./_dom":13}],11:[function(require,module,exports){
"use strict";

module.exports = {
  moveToTarget: function moveToTarget(x, y) {
    var fn = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

    var _this = this;

    var cb = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    var speedMod = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 1;

    this.update = function () {
      fn();
      var stopX = false;
      if (_this.x < x - 2) {
        _this.x += _this.speed * speedMod;
      } else if (_this.x > x + 2) {
        _this.x -= _this.speed * speedMod;
      } else {
        stopX = true;
      }

      var stopY = false;
      if (_this.y < y - 2) {
        _this.y += _this.speed * speedMod;
      } else if (_this.y > y + 2) {
        _this.y -= _this.speed * speedMod;
      } else {
        stopY = true;
      }

      if (stopX && stopY) {
        _this.update = function () {};
        cb();
      }
    };
  },
  pulse: function pulse(obj) {
    var _this2 = this;

    if (!this.animating) {
      this.animating = true;
      var scale = 0.8;
      var animationTimer = setInterval(function () {
        if (scale < 1.1) {
          scale += 0.1;
        } else {
          scale -= 0.1;
        }
        obj.style.transform = "scale(" + scale + ", " + scale + ")";
      }, 200);
      setTimeout(function () {
        clearInterval(animationTimer);
        _this2.animating = false;
      }, 600);
    }
  },
  wobble: function wobble(obj, _rotation) {
    var _this3 = this;

    if (!this.animating) {
      var rotation = _rotation;
      obj.style.transform = "rotate(" + rotation + "deg)";
      var animationTimer = setInterval(function () {
        if (rotation < 0) {
          rotation += _rotation;
        } else {
          rotation -= _rotation;
        }
        obj.style.transform = "rotate(" + rotation + "deg)";
      }, 100);
      setTimeout(function () {
        clearInterval(animationTimer);
        _this3.animating = false;
      }, 300);
    }
    this.animating = true;
  }
};

},{}],12:[function(require,module,exports){
(function (global){
'use strict';

var _dom = require('./_dom');

var _areColliding = function _areColliding(obj1, obj2) {
  if (!obj1.cya && !obj2.cya && obj1.x < obj2.x + obj2.width && obj1.x + obj1.width > obj2.x && obj1.y < obj2.y + obj2.height && obj1.height + obj1.y > obj2.y) {
    return true;
  }
};

module.exports = {
  playerEdge: function playerEdge(player) {
    var paddedWidth = global.width - global.padding - 2;
    var paddedHeight = global.height + global.gameSize - global.padding;

    if (player.right && player.x + player.width > paddedWidth) {
      player.x -= player.speed;
    }
    if (player.down && player.y + player.height > paddedHeight) {
      player.y -= player.speed;
    }
  },
  playerShell: function playerShell(player, shell, status) {
    if (player.left && (_areColliding(player, shell) || player.x < shell.x + shell.width - 10)) {
      player.x += player.speed;
      shell.contact();
    }
  },
  playerWater: function playerWater(player, water, status) {
    if (player.up && _areColliding(player, water.shoreline) && global.round < 5) {
      player.y += player.speed;
    }
    if (water.isSplashable && _areColliding(player, water.hitbox) && global.round > 0 && global.round < 3) {
      status.setMessage('splashin\' time', 'assets/wave.png');
      player.canSplash = true;
    } else if (water.isSplashable && global.round > 0 && player.canSplash) {
      player.canSplash = false;
      status.startStatus();
    }
  },
  bulletsErrbody: function bulletsErrbody(bullets, player, water, flock) {
    bullets.all.forEach(function (bullet) {
      if (!player.bullet && bullet.y > water.shoreline.y && !player.partnered && _areColliding(player, bullet)) {
        player.holdBullet(bullet);
        bullet.unassign();
      }
      if (bullet.state === 'landed' && _areColliding(bullet, water)) {
        bullet.state = 'wet';
        water.bullets.push(bullet);
      }
      if (flock) {
        flock.eagles.forEach(function (eagle) {
          if (_areColliding(eagle, bullet) && !eagle.pacified) {
            eagle.pacify(bullet.type);
            bullet.unassign();
            player.bullet = null;
          }
        });
        if (flock.finalEagle && !flock.finalEagle.locked && _areColliding(flock.finalEagle, bullet)) {
          flock.finalEagle.struck();
          player.bullet = null;
        }
      }
    });
  }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./_dom":13}],13:[function(require,module,exports){
'use strict';

var _animations = require('./_animations');

module.exports = {
  position: function position(el) {
    var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        _ref$left = _ref.left,
        left = _ref$left === undefined ? null : _ref$left,
        _ref$top = _ref.top,
        top = _ref$top === undefined ? null : _ref$top,
        _ref$width = _ref.width,
        width = _ref$width === undefined ? null : _ref$width,
        _ref$height = _ref.height,
        height = _ref$height === undefined ? null : _ref$height;

    if (left) {
      el.style.left = left + 'px';
    }
    if (top) {
      el.style.top = top + 'px';
    }
    if (width) {
      el.style.width = width + 'px';
    }
    if (height) {
      el.style.height = height + 'px';
    }
  },

  teardown: function teardown(el) {
    if (!this.cya) {
      while (el.firstChild) {
        el.removeChild(el.firstChild);
      }
      document.body.removeChild(el);
      this.update = function () {};
      this.draw = function () {};
    }
    this.cya = true;
  },

  buildGameDiv: function buildGameDiv() {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref2$parent = _ref2.parent,
        parent = _ref2$parent === undefined ? null : _ref2$parent,
        _ref2$id = _ref2.id,
        id = _ref2$id === undefined ? null : _ref2$id,
        _ref2$className = _ref2.className,
        className = _ref2$className === undefined ? null : _ref2$className,
        _ref2$src = _ref2.src,
        src = _ref2$src === undefined ? null : _ref2$src,
        _ref2$left = _ref2.left,
        left = _ref2$left === undefined ? null : _ref2$left,
        _ref2$top = _ref2.top,
        top = _ref2$top === undefined ? null : _ref2$top,
        _ref2$width = _ref2.width,
        width = _ref2$width === undefined ? null : _ref2$width,
        _ref2$height = _ref2.height,
        height = _ref2$height === undefined ? null : _ref2$height;

    var el = document.createElement('div');
    el.className += 'gameObjDiv ';

    if (id) {
      el.id = id;
    }
    if (className) {
      el.className += className;
    }
    if (src) {
      var img = document.createElement('img');
      img.className += 'gameObjImg';
      img.src = src;
      el.appendChild(img);
    }

    module.exports.position(el, { left: left, top: top, width: width, height: height });

    if (parent) {
      el.style.position = 'relative';
      parent.appendChild(el);
    } else {
      el.style.position = 'absolute';
      document.body.appendChild(el);
    }

    return el;
  }
};

},{"./_animations":11}],14:[function(require,module,exports){
'use strict';

module.exports.bindPlayerInputs = function (player) {
  document.addEventListener('keydown', function (e) {
    if (player.controllable) {
      switch (e.keyCode) {
        case 65:
        case 37:
          player.left = true;
          break;
        case 87:
        case 38:
          player.up = true;
          break;
        case 68:
        case 39:
          player.right = true;
          break;
        case 83:
        case 40:
          player.down = true;
          break;
      }
    }
  });

  document.addEventListener('keyup', function (e) {
    if (player.controllable) {
      switch (e.keyCode) {
        case 65:
        case 37:
          player.left = false;
          break;
        case 87:
        case 38:
          player.up = false;
          break;
        case 68:
        case 39:
          player.right = false;
          break;
        case 83:
        case 40:
          player.down = false;
          break;
      }
    }
  });

  document.addEventListener('keydown', function (e) {
    if (e.keyCode == 32 && (player.canSplash || player.bullet)) {
      player.fireBullet();
    }
  });
};

},{}]},{},[1]);
