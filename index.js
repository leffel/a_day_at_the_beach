import Game from './src/Game'

window.onload = function() {
  global.arcadeMode = false
  global.gameSize = global.arcadeMode ? 700 : 500
  global.padding = global.arcadeMode ? 10 : 5

  global.left = (window.innerWidth - global.gameSize) * 0.5
  global.width = global.left + global.gameSize
  global.height = (window.innerHeight - global.gameSize) * 0.3

  const background = document.createElement("img")
  background.id = 'background'
  background.src = "assets/baileys_bg.png"
  background.style.width = `${global.gameSize}px`
  background.style.height = `${global.gameSize}px`
  background.style.padding = `${global.padding}px`

  const container = document.createElement("div")
  container.style.width = `${width - 10}px`

  const anotherContainer = document.createElement("div")
  anotherContainer.id = 'anotherContainer'
  anotherContainer.style.left = `${global.left}px`
  anotherContainer.style.top = `${global.height}px`

  anotherContainer.appendChild(background)
  container.appendChild(anotherContainer)
  document.body.appendChild(container)

  // still need onload if this is loaded with css?
  background.onload = function(){
    new Game()
  }
}
